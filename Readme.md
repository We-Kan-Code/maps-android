# Map Boiler

### This project is indented to demonstrate how to cancel ongoing api calls using coroutine job.

## Libraries used for Boiler plate

* Android X
* Lifecycle extension(viewmodel & livedata)
* Dagger 2
* Retrofit
* Moshi
* Testing framework(JUnit,Robolectic,Espresso,UIAutomator)
* Coroutine
* Glide
* Product flavours

## Feature covered

1. Fuel stations marked as pin on map when map is idle.
1. Loading stations with in the radius of the user zoom level.
1. Cancellation of ongoing api call / coroutine job if the user swipes away from the current position on map.