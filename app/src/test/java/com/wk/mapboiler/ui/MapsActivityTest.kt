package com.wk.mapboiler.ui

import com.wk.mapboiler.utils.AppUtils
import org.junit.Test

import org.junit.Assert.*

class MapsActivityTest {

    @Test
    fun metersToMiles_Success() {
        var miles = AppUtils.metersToMiles(1000.0)
        assertEquals(0.6213711900070149,miles,0.0)
    }

    @Test
    fun metersToMiles_Fail() {
        var miles = AppUtils.metersToMiles(1000.0)
        assertNotEquals(1609.3440057765,miles,0.0)
    }
}