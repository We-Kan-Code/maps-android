package com.wk.mapboiler.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.wk.mapboiler.R
import android.graphics.Color
import com.google.android.gms.maps.model.CircleOptions
import com.google.maps.android.SphericalUtil
import android.R.attr.radius
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import dagger.android.AndroidInjection
import javax.inject.Inject
import androidx.core.app.ComponentActivity.ExtraData
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import com.wk.mapboiler.utils.AppUtils


class MapsActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap

    private var currentZoom = 12.0f
    private val minZoom = 10.0f
    private val maxZoom = 18.0f

    //initializing dashboard view model factory
    @Inject
    lateinit var mapsModelFactory:MapsModelFactory
    lateinit var mapsViewModel: MapsViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(com.wk.mapboiler.R.layout.activity_maps)
        AndroidInjection.inject(this)
        //initializing dashboard view model
        mapsViewModel = ViewModelProviders.of(this,mapsModelFactory).get(MapsViewModel::class.java)

        //observing the viewmodel for dashboard api result change
        mapsViewModel.mapsResult.observe(this@MapsActivity, Observer{
            val mapResult = it ?: return@Observer

            if (mapResult .error != null) {
                showToast(mapResult .error)
            }
            if (mapResult .errorCode != null) {
                showToast(mapResult .errorCode)
            }

            mapResult.stationList?.fuelStations?.forEach { it ->
                mMap.addMarker(MarkerOptions().position(LatLng(it.latitude,it.longitude)).title(it.stationName))
            }

            mapsViewModel.idleResource.decrement()
        })
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
            .findFragmentById(com.wk.mapboiler.R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        mMap.setContentDescription("MAP READY")
        val us = LatLng(39.74311, -105.152332)
        mMap.setMinZoomPreference(minZoom)
        mMap.setMaxZoomPreference(maxZoom)
        mMap.addMarker(MarkerOptions().position(us))
        mMap.uiSettings.isZoomControlsEnabled = true
        mMap.moveCamera(
            CameraUpdateFactory.newLatLngZoom(
                us, currentZoom
            ))
        mMap.setOnMarkerClickListener{marker ->
            if (marker.isInfoWindowShown) {
                marker.hideInfoWindow()
            } else {
                marker.showInfoWindow()
            }
            true
        }
        mMap.setOnCameraIdleListener{
            mapsViewModel.idleResource.increment()
            val latLng = mMap.cameraPosition.target
            currentZoom = mMap.cameraPosition.zoom
            var bound = mMap.projection.visibleRegion.latLngBounds
            var cor1 = bound.northeast
            var cor2 = bound.southwest
            var cor3 = LatLng(cor2.latitude,cor1.longitude)
            var cor4 = LatLng(cor1.latitude,cor2.longitude)
            val height = SphericalUtil.computeDistanceBetween(cor1, cor3)
            val width = SphericalUtil.computeDistanceBetween(cor1, cor4)
                mMap.clear()
                mMap.addMarker(MarkerOptions().position(latLng).title("My Location")).showInfoWindow()
            var circle = mMap.addCircle(CircleOptions().center(latLng).radius((width/2)*0.9)
                .strokeColor(Color.RED))
            var meters_px = 156543.03392 * Math.cos(latLng.latitude * Math.PI / 180) / Math.pow(2.toDouble(), currentZoom.toDouble())

            // here mMap is my GoogleMap object
//            mMap.addMarker(MarkerOptions().position(point))
            mapsViewModel.loadAllNearByStationData(latLng,AppUtils.metersToMiles((width/2)*0.9))
//            mMap.addMarker(MarkerOptions().position(LatLng(circle.center.latitude,latLng.longitude)))
            Log.i("setOnCameraIdleListener","$meters_px  ${AppUtils.metersToMiles((width/2)*0.9)} ${(width/2)*0.9} $currentZoom ${latLng.latitude } ${latLng.longitude }")
        }

        mMap.setOnCameraMoveStartedListener {
            when(it){
                GoogleMap.OnCameraMoveStartedListener.REASON_API_ANIMATION,
                GoogleMap.OnCameraMoveStartedListener.REASON_GESTURE -> {
                    mapsViewModel.cancelJob()
                }
                GoogleMap.OnCameraMoveStartedListener.REASON_DEVELOPER_ANIMATION -> {

                }
            }
        }
    }

    // toast to show error with res id
    fun showToast(@StringRes errorString: Int) {
        Toast.makeText(applicationContext, errorString, Toast.LENGTH_SHORT).show()
    }
    // toast to show error with string
    fun showToast(errorString: String) {
        Toast.makeText(applicationContext, errorString, Toast.LENGTH_SHORT).show()
    }
}
