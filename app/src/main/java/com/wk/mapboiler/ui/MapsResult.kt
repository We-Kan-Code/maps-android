package com.wk.mapboiler.ui

import com.wk.mapboiler.data.remote.Result
import com.wk.mapboiler.data.remote.reqres.nrel.NrelNearByResponse

/**
 * Data validation for load user Api of the dashboard.
 */
data class MapsResult(
    val stationList: NrelNearByResponse? = null,
    val errorCode: Int? = null,
    val error: String? = null
)
