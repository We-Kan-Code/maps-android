package com.wk.mapboiler.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.test.espresso.idling.CountingIdlingResource
import com.google.android.gms.maps.model.LatLng
import com.wk.mapboiler.R
import com.wk.mapboiler.data.remote.MapsRepository
import com.wk.mapboiler.data.remote.Result
import com.wk.mapboiler.data.remote.reqres.nrel.NrelNearByResponse
import kotlinx.coroutines.*
import javax.inject.Inject
import kotlin.coroutines.CoroutineContext

/**
 * View model class for dashboard
 */
class MapsViewModel @Inject constructor(private val mapsRepository: MapsRepository ):ViewModel(),CoroutineScope  {
    var idleResource = CountingIdlingResource("dash")
    /**
     * livedata to observe load user api result data
     */
    private val _mapsResult = MutableLiveData<MapsResult>()
    val mapsResult: LiveData<MapsResult> = _mapsResult

    private var loadJob: Job = Job()
    override val coroutineContext: CoroutineContext
        get() = loadJob + Dispatchers.IO

    /**
     * function to load user data
     */
    fun loadAllNearByStationData(latlng : LatLng,radius:Double){
        idleResource.increment()
        try {
            loadJob = GlobalScope.launch {
                if(isActive) {
                    var result = mapsRepository.loadStationsNearBy(
                        latlng.latitude.toString(),
                        latlng.longitude.toString(),
                        radius
                    )
                        withContext(Dispatchers.Main) {
                                when (result) {
                                    is Result.Success -> {
                                            var stations = result
                                            _mapsResult.value = MapsResult(stationList = stations.data)
                                    }
                                    is Result.Error -> {
                                            _mapsResult.value = MapsResult(error = result.msg)
                                    }
                                    else -> {
                                            _mapsResult.value =
                                                MapsResult(errorCode = R.string.unknown_error)
                                    }
                                }
                        }
                }
            }
        } finally {
            idleResource.decrement()
        }

    }

    fun cancelJob(){
        loadJob.cancel()
        mapsRepository.cancelStationLoad()
    }
}