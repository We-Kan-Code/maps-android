package com.wk.mapboiler.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.wk.mapboiler.ui.MapsViewModel
import javax.inject.Inject

/**
 * ViewModel provider factory to instantiate
 */
class MapsModelFactory @Inject constructor(private val mapsViewModel : MapsViewModel): ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(MapsViewModel::class.java)) {
            return mapsViewModel as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}
