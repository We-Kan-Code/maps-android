package com.wk.mapboiler.data.remote.reqres.nrel


import com.squareup.moshi.Json

data class HY(
    @Json(name = "total")
    val total: Int
)