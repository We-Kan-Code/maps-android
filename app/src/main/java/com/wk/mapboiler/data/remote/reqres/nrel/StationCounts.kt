package com.wk.mapboiler.data.remote.reqres.nrel


import com.squareup.moshi.Json

data class StationCounts(
    @Json(name = "fuels")
    val fuels: Fuels,
    @Json(name = "total")
    val total: Int
)