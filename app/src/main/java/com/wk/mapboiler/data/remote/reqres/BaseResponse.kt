package com.wk.mapboiler.data.remote.reqres

import com.squareup.moshi.Json

open class BaseResponse (
    @Json(name = "errors")
    var errors: ApiErrors? = null
)