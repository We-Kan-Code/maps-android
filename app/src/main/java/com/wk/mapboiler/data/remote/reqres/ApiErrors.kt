package com.wk.mapboiler.data.remote.reqres


import com.squareup.moshi.Json

data class ApiErrors(
    @Json(name = "messages")
    val messages: List<String>
)