package com.wk.mapboiler.data.remote.reqres.nrel

enum class VehicleTypes {
    ALL, BD, CNG, E85, ELEC, HY, LNG, LPG
}