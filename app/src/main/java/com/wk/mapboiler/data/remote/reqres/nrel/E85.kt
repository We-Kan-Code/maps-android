package com.wk.mapboiler.data.remote.reqres.nrel


import com.squareup.moshi.Json

data class E85(
    @Json(name = "total")
    val total: Int
)