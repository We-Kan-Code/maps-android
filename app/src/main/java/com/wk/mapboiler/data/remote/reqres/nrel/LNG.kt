package com.wk.mapboiler.data.remote.reqres.nrel


import com.squareup.moshi.Json

data class LNG(
    @Json(name = "total")
    val total: Int
)