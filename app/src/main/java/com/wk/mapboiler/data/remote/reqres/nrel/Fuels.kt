package com.wk.mapboiler.data.remote.reqres.nrel


import com.squareup.moshi.Json

data class Fuels(
    @Json(name = "BD")
    val bD: BD,
    @Json(name = "CNG")
    val cNG: CNG,
    @Json(name = "E85")
    val e85: E85,
    @Json(name = "ELEC")
    val eLEC: ELEC,
    @Json(name = "HY")
    val hY: HY,
    @Json(name = "LNG")
    val lNG: LNG,
    @Json(name = "LPG")
    val lPG: LPG
)