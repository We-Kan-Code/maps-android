package com.wk.mapboiler.data.remote.reqres.nrel


import com.squareup.moshi.Json

data class NrelNearByResponse(
    @Json(name = "fuel_stations")
    val fuelStations: List<FuelStation>,
    @Json(name = "latitude")
    val latitude: Double,
    @Json(name = "location_country")
    val locationCountry: String?,
    @Json(name = "longitude")
    val longitude: Double,
    @Json(name = "offset")
    val offset: Int,
    @Json(name = "precision")
    val precision: Precision?,
    @Json(name = "station_counts")
    val stationCounts: StationCounts,
    @Json(name = "station_locator_url")
    val stationLocatorUrl: String,
    @Json(name = "total_results")
    val totalResults: Int
)