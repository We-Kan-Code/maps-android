package com.wk.mapboiler.data.remote.reqres.nrel


import com.squareup.moshi.Json

data class Precision(
    @Json(name = "name")
    val name: String,
    @Json(name = "types")
    val types: List<String>,
    @Json(name = "value")
    val value: Int
)