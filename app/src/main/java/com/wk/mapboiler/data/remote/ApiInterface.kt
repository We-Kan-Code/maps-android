package com.wk.mapboiler.data.remote

import com.wk.mapboiler.data.remote.reqres.nrel.NrelNearByResponse
import retrofit2.Call
import retrofit2.http.*

interface ApiInterface{
    /**
     * Station near by API
     */
    @Headers("Accept: application/json")
    @GET("api/alt-fuel-stations/v1/nearest.json")
    fun getStationsNearBy(@Query(value = "api_key") api_key: String,
                          @Query(value = "latitude") latitude: String,
                          @Query(value = "longitude") longitude: String,
                          @Query(value = "radius") radius: Double,
                       @Query(value = "fuel_type") fuel_type: String
                          ): Call<NrelNearByResponse>
}