package com.wk.mapboiler.data.remote.reqres.nrel


import com.squareup.moshi.Json

data class ELEC(
    @Json(name = "stations")
    val stations: Stations?,
    @Json(name = "total")
    val total: Int
)