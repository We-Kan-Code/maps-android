package com.wk.mapboiler.data.remote

import android.util.Log
import com.squareup.moshi.Moshi
import com.wk.mapboiler.R
import com.wk.mapboiler.data.remote.ApiInterface
import com.wk.mapboiler.data.remote.reqres.BaseResponse
import com.wk.mapboiler.data.remote.reqres.nrel.NrelNearByResponse
import com.wk.mapboiler.data.remote.reqres.nrel.VehicleTypes
import com.wk.mapboiler.utils.ApiConstants
import retrofit2.Call
import javax.inject.Inject

/**
 * Repository Class that handles api call.
 */

class MapsRepository @Inject constructor(private val apiInterface: ApiInterface) {

    @Inject
    lateinit var moshi: Moshi
    var request:Call<NrelNearByResponse>? =  null
    /**
     * function to get stations by latlng from server
     */
    fun loadStationsNearBy(lat:String,lng:String,radius:Double): Result<NrelNearByResponse>?{
        try {
            request = apiInterface.getStationsNearBy(ApiConstants.APIKEY,lat,lng,radius,VehicleTypes.ALL.name.toLowerCase())
            val response = request?.execute()
            Log.i("station response ", response.toString())
            return when {
                response?.body() is NrelNearByResponse -> {
                    val userData = response.body()
                    Log.i("station data", userData.toString())
                    Result.Success(userData!!)
                }
                response?.errorBody()!=null -> {
                    val adapter = moshi.adapter(BaseResponse::class.java).lenient()
                    val error = adapter.fromJson(response.errorBody()?.string()!!)
                    Result.Error(msg = error?.errors?.messages?.get(0),statusCode = response.code())
                }
                else -> Result.Error(resId = R.string.unknown_error)
            }
        } catch (e: Throwable) {
            return if(request?.isCanceled !=null && request?.isCanceled == true) {
                Result.Error()
            }else{
                e.printStackTrace()
                Result.Error(resId = R.string.unknown_error)
            }
        }
    }

    fun cancelStationLoad(){
        request?.cancel()
    }
}