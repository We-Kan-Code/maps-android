package com.wk.mapboiler.data.remote.reqres.nrel


import com.squareup.moshi.Json

data class EvNetworkIds(
    @Json(name = "posts")
    val posts: List<String>?,
    @Json(name = "station")
    val station: List<String>?
)