package com.wk.mapboiler.utils

import android.content.SharedPreferences
import com.squareup.moshi.Moshi
import javax.inject.Inject

/**
 * class to provide functions to access shared preference values
 */
class PrefUtils @Inject constructor(){

    @Inject
    lateinit var appPref:SharedPreferences

    @Inject
    lateinit var moshi: Moshi


}