package com.wk.mapboiler.utils

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import android.util.Patterns
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat.getSystemService
import java.security.MessageDigest

class AppUtils{
    companion object{

        fun metersToMiles(meters: Double): Double {
            return meters / 1609.3440057765
        }
    }
}

