package com.wk.mapboiler.utils

/**
 * Constants for app/intent keys
 */
class Constants{
    companion object{
        const val PERMISSION_REQUSET_CODE = 200
    }
}

/**
 * Constants for shared preferences key
 */
class PrefKeys {
    companion object{
        val PREFNAME = "app_pref"
    }
}

/**
 * Constants for network api keys
 */
class ApiConstants{
    companion object{
        const val APIKEY = "DEMO_KEY"
    }
}

