package com.wk.mapboiler.di.component

import com.wk.mapboiler.App
import com.wk.mapboiler.di.module.ActivityBuldersModule
import com.wk.mapboiler.di.module.AppModule
import com.wk.mapboiler.di.module.NetModule
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

/**
 * class to define the components that are provided for the app with the defined list of modules
 */
@Singleton
@Component(
    modules = [(AndroidInjectionModule::class),(AppModule::class),(NetModule::class),(ActivityBuldersModule::class)]
)
interface AppComponent {
    fun inject(app: App)
}