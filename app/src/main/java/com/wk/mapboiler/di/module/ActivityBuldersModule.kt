package com.wk.mapboiler.di.module

import com.wk.mapboiler.ui.MapsActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * class to provide the injection for the activities
 */
@Module
abstract class ActivityBuldersModule {

    @ContributesAndroidInjector
    abstract fun contributeMapsActivity(): MapsActivity
}