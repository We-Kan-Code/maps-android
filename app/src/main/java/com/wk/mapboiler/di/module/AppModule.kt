package com.wk.mapboiler.di.module

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import com.wk.mapboiler.utils.PrefKeys
import dagger.Module
import dagger.Provides
import javax.inject.Singleton
/**
 * class to provide injection for application level access
 */
@Module
class AppModule (val app:Application){
    @Provides
    @Singleton
    fun provideApplication(): Application = app

    @Provides
    @Singleton
    fun provideSharedPreferences(app: Application):SharedPreferences = app.getSharedPreferences(
        PrefKeys.PREFNAME, Context.MODE_PRIVATE)

}