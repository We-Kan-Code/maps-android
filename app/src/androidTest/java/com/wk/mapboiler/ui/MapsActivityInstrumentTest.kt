package com.wk.mapboiler.ui

import android.util.Log
import androidx.test.espresso.IdlingRegistry
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import androidx.test.uiautomator.By
import androidx.test.uiautomator.UiDevice
import androidx.test.uiautomator.UiSelector
import androidx.test.uiautomator.Until
import org.junit.After
import org.junit.Before

import org.junit.Assert.*
import org.junit.Rule
import org.junit.Test

class MapsActivityInstrumentTest {

    @get:Rule
    val activityMapRule = ActivityTestRule(MapsActivity::class.java)

    @Before
    fun setUp() {

    }

    @Test
    fun testLoadStations(){
        //My Location
        val device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation())

        val child = device.findObject(
            UiSelector()
                .descriptionContains("Location"))

        Log.i("test",child.contentDescription)
        device.wait(
            Until.hasObject(By.pkg(device.launcherPackageName).depth(0)),
            5000)
        val mills = device.findObject(
            UiSelector()
                .descriptionContains("Mills"))
        mills.click()
        assert(child.contentDescription.startsWith("Colorado",false))
    }

    @Test
    fun testSwipeMapToStations(){
        //My Location
        val device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation())

        val map = device.findObject(
            UiSelector()
                .descriptionContains("MAP READY"))
        map.swipeRight(2)
        map.swipeLeft(2)
        Log.i("test",map.contentDescription)
        device.wait(
            Until.hasObject(By.pkg(device.launcherPackageName).depth(0)),
            5000)
        val childView =  map.getChild(UiSelector().descriptionContains("FC"))
        childView.click()
        assert(childView.contentDescription.startsWith("D",true))
    }

    @After
    fun tearDown() {

    }
}