# Change log

### All the changes to the project will be recorded in this file with respective version number

## v1.0

## Added

1. Libraries 
	* Android X
	* Lifecycle extension(viewmodel & livedata)
	* Dagger 2
	* Retrofit
	* Moshi
	* Testing framework(JUnit,Robolectic,Espresso)
	* Room
	* Realm
	* Coroutine
	* Glide
	* Product flavours

1. Features Added
    * Fuel stations marked as pin on map when map is idle.
    * Loading stations with in the radius of the user zoom level.
    * Cancellation of ongoing api call / coroutine job if the user swipes away from the current position on map.